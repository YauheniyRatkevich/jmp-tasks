package com.epam.jmp.ratkevich.controller;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.stream.Collectors;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.epam.jmp.fileinfo.FileInfoWS;
import com.epam.jmp.fileinfo.GetAllFilesResponse;
import com.epam.jmp.fileinfo.GetFileRequest;
import com.epam.jmp.fileinfo.GetFileResponse;
import com.epam.jmp.fileinfo.MarkFileAsExpiredRequest;
import com.epam.jmp.fileinfo.UploadFileRequest;
import com.epam.jmp.fileinfo.UploadFileResponse;
import com.epam.jmp.ratkevich.dto.FileInfoDTO;
import com.epam.jmp.ratkevich.service.FileService;

@Endpoint
public class FileEndpoint {

	private static final String NAMESPACE_URI = "http://jmp.epam.com/fileInfo";

	@Autowired
	private FileService fileService;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllFilesRequest")
	@ResponsePayload
	public GetAllFilesResponse getAllFiles() {
		GetAllFilesResponse filesResponse = new GetAllFilesResponse();
		filesResponse.getFile().addAll(fileService.getAll().stream().map(fileInfo -> {
			return fileInfoDTOtoWS(fileInfo);
		}).collect(Collectors.toList()));
		return filesResponse;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getFileRequest")
	@ResponsePayload
	public GetFileResponse getFile(@RequestPayload GetFileRequest request) {
		FileInfoDTO fileInfo = fileService.findOne(request.getId());
		FileInfoWS fileInfoWS = fileInfoDTOtoWS(fileInfo);

		GetFileResponse fileResponse = new GetFileResponse();
		fileResponse.setFile(fileInfoWS);
		return fileResponse;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "uploadFileRequest")
	@ResponsePayload
	public UploadFileResponse uploadFile(@RequestPayload UploadFileRequest request) {
		UploadFileResponse uploadFileResponse = new UploadFileResponse();
		FileInfoWS fileInfoWS = request.getFile();
		if (fileInfoWS != null) {
			FileInfoDTO fileInfo = new FileInfoDTO();
			BeanUtils.copyProperties(fileInfoWS, fileInfo);

			fileInfo.setExpirationDate(xmlGregorianCalendarToDate(fileInfoWS.getExpirationDate()));
			fileInfo.setUploadDate(xmlGregorianCalendarToDate(fileInfoWS.getUploadDate()));

			FileInfoDTO savedFile = fileService.save(fileInfo);
			uploadFileResponse.setId(savedFile.getId());
			return uploadFileResponse;
		}
		return uploadFileResponse;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "markFileAsExpiredRequest")
	@ResponsePayload
	public void markAsExpired(@RequestPayload MarkFileAsExpiredRequest request) {
		Long fileId = request.getId();
		if (fileId != null) {
			fileService.markAsExpired(fileId);
		}
	}

	private FileInfoWS fileInfoDTOtoWS(FileInfoDTO fileInfo) {
		FileInfoWS fileInfoWS = new FileInfoWS();
		if (fileInfo != null) {
			BeanUtils.copyProperties(fileInfo, fileInfoWS);
			fileInfoWS.setId(fileInfo.getId());
			fileInfoWS.setUploadDate(dateToXmlGregorianCalendar(fileInfo.getUploadDate()));
			fileInfoWS.setExpirationDate(dateToXmlGregorianCalendar(fileInfo.getExpirationDate()));
		}
		return fileInfoWS;
	}

	private XMLGregorianCalendar dateToXmlGregorianCalendar(LocalDateTime date) {
		if (date != null) {
			try {
				GregorianCalendar calendar = new GregorianCalendar();
				calendar.setTime(Date.from(date.toInstant(ZoneOffset.UTC)));
				return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
			} catch (DatatypeConfigurationException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	private LocalDateTime xmlGregorianCalendarToDate(XMLGregorianCalendar xmlGregorianCalendar) {
		if (xmlGregorianCalendar != null) {
			Date utilDate = xmlGregorianCalendar.toGregorianCalendar().getTime();
			return LocalDateTime.ofInstant(utilDate.toInstant(), ZoneId.systemDefault());
		}
		return null;
	}
}
