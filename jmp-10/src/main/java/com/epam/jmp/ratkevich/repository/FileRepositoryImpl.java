package com.epam.jmp.ratkevich.repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Repository;

import com.epam.jmp.ratkevich.entity.FileInfo;

@Repository
public class FileRepositoryImpl implements FileRepository {

	private final List<FileInfo> files = new ArrayList<>();

	private AtomicLong idSequence = new AtomicLong();

	@PostConstruct
	private void init() {
		FileInfo fileInfo = new FileInfo();
		fileInfo.setData(new byte[] { 1, 2, 3 });
		fileInfo.setExpirationDate(LocalDateTime.now().plusDays(1L));
		fileInfo.setExpired(true);
		fileInfo.setFileName("notes.txt");
		fileInfo.setUploadDate(LocalDateTime.now());
		save(fileInfo);
	}

	@Override
	public FileInfo save(FileInfo fileInfo) {
		fileInfo.setId(idSequence.addAndGet(1));
		files.add(fileInfo);
		return fileInfo;
	}

	@Override
	public FileInfo findOne(Long id) {
		return files.stream().filter(file -> file.getId().equals(id)).findFirst().orElse(null);
	}

	@Override
	public LocalDateTime getUploadDate(Long id) {
		Optional<FileInfo> fileOptional = files.stream().filter(file -> file.getId().equals(id)).findFirst();
		return fileOptional.isPresent() ? fileOptional.get().getUploadDate() : null;
	}

	@Override
	public List<FileInfo> findAll() {
		return Collections.unmodifiableList(files);
	}

	@Override
	public void markAsExpired(Long id) {
		files.stream().filter(file -> file.getId().equals(id)).findFirst().ifPresent(file -> file.setExpired(true));
	}

}
