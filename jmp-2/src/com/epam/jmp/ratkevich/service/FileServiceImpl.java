package com.epam.jmp.ratkevich.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FileServiceImpl implements FileService {

	protected final int threadCountLimit = 10;

	private String destFolder;

	private Lock lock = new ReentrantLock();

	public FileServiceImpl(String destFolder) {
		this.destFolder = destFolder;
	}

	@Override
	public void markAsExpired(Long id) {
		throw new UnsupportedOperationException();
	}

	public void save(Path sourceFile) {
		try {
			Path newdir = Paths.get(destFolder);
			if (!Files.exists(newdir) && lock.tryLock()) {
				try {
					Files.createDirectory(newdir);
				} finally {
					lock.unlock();
				}
			}
			Files.copy(sourceFile, newdir.resolve(sourceFile.getFileName()), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void saveAll(Path sourceDir) throws IOException {
		Files.walk(sourceDir).filter(Files::isRegularFile).parallel().forEach(this::save);
	}

}
