package com.epam.jmp.ratkevich.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class FileServiceImplExecutorService extends FileServiceImpl {

	public FileServiceImplExecutorService(String destFolder) {
		super(destFolder);
	}

	@Override
	public void saveAll(Path sourceDir) throws IOException {
		final List<Thread> sourceFiles = Files.walk(sourceDir).filter(Files::isRegularFile)
				.map(sourceFile -> new Thread(() -> save(sourceFile)))
				.collect(Collectors.toList());

		final ExecutorService executorService = Executors.newFixedThreadPool(threadCountLimit);
		sourceFiles.forEach(sourceFileThread -> {
			executorService.submit(sourceFileThread);
		});
		executorService.shutdown();
	}

}
