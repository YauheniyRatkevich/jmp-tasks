package com.epam.jmp.ratkevich;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import com.epam.jmp.ratkevich.service.FileService;
import com.epam.jmp.ratkevich.service.FileServiceImplExecutorService;

public class Main {

	private static final String SOURCE_FOLDER = "SOURCE";

	private static final String DEST_FOLDER = "DEST";

	public static void main(String[] args) {
		try {
			FileService fileService;
			deleteDirectory(new File(DEST_FOLDER)); // cleanup DEST dir

			// fileService = new FileServiceImpl(DEST_FOLDER);
			fileService = new FileServiceImplExecutorService(DEST_FOLDER);
			// fileService = new FileServiceImplSemaphore(DEST_FOLDER);
			fileService.saveAll(Paths.get(SOURCE_FOLDER));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private static void deleteDirectory(File dir) {
		if (dir.exists() && dir.isDirectory()) {
			String[] files = dir.list();
			for (int i = 0, len = files.length; i < len; i++) {
				File f = new File(dir, files[i]);
				if (f.isDirectory()) {
					deleteDirectory(f);
				} else {
					f.delete();
				}
			}
			dir.delete();
		}
	}
}
