package com.epam.jmp.ratkevich.repository;

/**
 * Contains operations for manipulations with files in DB (CRUD operations and
 * some specific operations).
 *
 */
public interface FileRepository { // extends CrudRepository, Spring-based

    /**
     * Sets expired flag to true in DB for file with the specified id.
     * 
     * @param id
     *            - file id.
     */
    void markAsExpired(Long id);

}
