package com.epam.jmp.ratkevich.service;

import java.util.List;

import com.epam.jmp.ratkevich.dto.AbstractDomainDTO;

/**
 * Contains CRUD operations for {@link AbstractDomainDTO}
 *
 * @param <T>
 *            - real class representative of {@link AbstractDomainDTO}.
 */
public interface CrudService<T extends AbstractDomainDTO<?>> {

    List<T> getAll();

    T getById(Long id);

    T update(T t);

    T save(T t);

    T delete(T t);
}
