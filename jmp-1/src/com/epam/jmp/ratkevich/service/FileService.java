package com.epam.jmp.ratkevich.service;

import com.epam.jmp.ratkevich.dto.AbstractDomainDTO;

/**
 * Contains operations for manipulations with files (CRUD operations and some
 * specific operations).
 *
 */
public interface FileService extends CrudService<AbstractDomainDTO<?>> {

    /**
     * Marks file as expired by id.
     * 
     * @param id
     *            - file id.
     */
    void markAsExpired(Long id);
}
