package com.epam.jmp.ratkevich.dto;

import java.time.LocalDateTime;

/**
 * Contains information about a file within its metadata.
 *
 */
public class FileInfoDTO extends AbstractDomainDTO<Long> {

    private String name;

    private String extension;

    private LocalDateTime uploadDate;

    private LocalDateTime expirationDate;

    private boolean isExpired;

    private byte[] data;

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getExtension() {
	return extension;
    }

    public void setExtension(String extension) {
	this.extension = extension;
    }

    public LocalDateTime getUploadDate() {
	return uploadDate;
    }

    public void setUploadDate(LocalDateTime uploadDate) {
	this.uploadDate = uploadDate;
    }

    public LocalDateTime getExpirationDate() {
	return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
	this.expirationDate = expirationDate;
    }

    public byte[] getData() {
	return data;
    }

    public void setData(byte[] data) {
	this.data = data;
    }

    public boolean isExpired() {
	return isExpired;
    }

    public void setExpired(boolean isExpired) {
	this.isExpired = isExpired;
    }
}