package com.epam.jmp.ratkevich.service;

import java.time.LocalDateTime;

import com.epam.jmp.ratkevich.dto.FileInfoDTO;

/**
 * Contains operations for manipulations with files (CRUD operations and some specific operations).
 *
 */
public interface FileService extends CrudService<FileInfoDTO> {

	/**
	 * Marks file as expired by id.
	 * 
	 * @param id - file id.
	 */
	void markAsExpired(Long id);

	/**
	 * @return an file's upload date
	 */
	LocalDateTime getUploadDate(Long id);

}
