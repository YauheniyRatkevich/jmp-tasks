package com.epam.jmp.ratkevich.service.validation;

import java.util.Arrays;

public final class FileExtensionValidator {

	private FileExtensionValidator() {
	}

	private static final String[] ALLOWED_EXTENSIONS = new String[] { "doc", "docx", "pdf", "txt", "jpeg", "jpg", "png", "bmp", "zip" };

	/**
	 * @return true if valid
	 */
	public static boolean validate(String extension) {
		return Arrays.asList(ALLOWED_EXTENSIONS).contains(extension);
	}
}
