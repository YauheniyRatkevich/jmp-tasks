package com.epam.jmp.ratkevich.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.epam.jmp.ratkevich.converter.FileMetaDataConverter;
import com.epam.jmp.ratkevich.dto.FileInfoDTO;
import com.epam.jmp.ratkevich.entity.FileInfo;
import com.epam.jmp.ratkevich.repository.FileRepository;
import com.epam.jmp.ratkevich.service.validation.FileInfoValidator;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LocalDateTime.class, FileServiceImpl.class })
// I know that it's bad to mock types you're not own, just for example
public class FileServiceTest {

	private static final Long FILE_ID = 1L;

	private static final LocalDateTime DATE_TIME = LocalDateTime.now();

	private static final int EXPIRATION_IN_DAYS = 1;

	@InjectMocks
	private FileServiceImpl fileService;

	@Mock
	private FileRepository fileRepository;

	@Mock(answer = Answers.CALLS_REAL_METHODS)
	private FileMetaDataConverter fileConverter;

	@Mock
	private FileInfoValidator fileInfoValidator;

	@Test
	public void shouldSaveFileInfo() {
		mockStatic(LocalDateTime.class);
		when(LocalDateTime.now()).thenReturn(DATE_TIME);

		FileInfoDTO sourceFileDTO = new FileInfoDTO();
		sourceFileDTO.setFileName("file name");
		sourceFileDTO.setData(new byte[] { -1, 1, 2 });

		final FileInfo fileInfo = new FileInfo();
		fileInfo.setFileName(sourceFileDTO.getFileName());
		fileInfo.setUploadDate(DATE_TIME);
		fileInfo.setExpirationDate(DATE_TIME.plusDays(EXPIRATION_IN_DAYS));
		fileInfo.setExpired(false);
		fileInfo.setData(sourceFileDTO.getData());

		fileService.save(sourceFileDTO);
		verify(fileInfoValidator).validate(sourceFileDTO);
		verify(fileRepository).save(fileInfo);
	}

	@Test
	public void shouldFindFileInfoById() {
		final FileInfo fileInfo = new FileInfo();
		fileInfo.setId(FILE_ID);
		when(fileRepository.findOne(FILE_ID)).thenReturn(fileInfo);

		FileInfoDTO fileInfoDTO = fileService.findOne(FILE_ID);
		verify(fileConverter).convert(fileInfo);
		assertEquals(FILE_ID, fileInfoDTO.getId());
	}

	@Test
	public void shouldReturnAllFileInfos() {
		final FileInfo fileInfo = new FileInfo();
		fileInfo.setId(FILE_ID);
		when(fileRepository.findAll()).thenReturn(Arrays.asList(fileInfo));

		List<FileInfoDTO> fileInfos = fileService.getAll();
		assertEquals(fileInfo.getId(), fileInfos.get(0).getId());
	}

	@Test
	public void shouldMarkAsExpired() {
		fileService.markAsExpired(FILE_ID);
		verify(fileRepository).markAsExpired(FILE_ID);
	}

	@Test
	public void shouldReturnUploadDate() {
		when(fileRepository.getUploadDate(FILE_ID)).thenReturn(DATE_TIME);
		assertEquals(DATE_TIME, fileService.getUploadDate(FILE_ID));
	}
}
