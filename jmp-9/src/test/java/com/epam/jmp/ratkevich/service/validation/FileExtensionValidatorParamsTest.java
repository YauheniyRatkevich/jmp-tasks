package com.epam.jmp.ratkevich.service.validation;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class FileExtensionValidatorParamsTest {

	private static final String[] ALLOWED_EXTENSIONS = new String[] { "doc", "docx", "pdf", "txt", "jpeg", "jpg", "png", "bmp", "zip" };

	@Test
	@Parameters({ "doc", "docx", "pdf", "txt", "jpeg", "NON_SUPPORTED" })
	public void shouldValidateExtension(String extension) {
		assertEquals(testValidity(extension), FileExtensionValidator.validate(extension));
	}

	public boolean testValidity(String extension) {
		return Arrays.asList(ALLOWED_EXTENSIONS).contains(extension);
	}

}
