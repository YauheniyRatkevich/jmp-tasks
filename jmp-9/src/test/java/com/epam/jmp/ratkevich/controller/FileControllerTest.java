package com.epam.jmp.ratkevich.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.jmp.ratkevich.dto.FileInfoDTO;
import com.epam.jmp.ratkevich.service.FileService;

@RunWith(SpringRunner.class)
@WebMvcTest(FileController.class)
public class FileControllerTest {

	private static final String FILES_URL_PREFIX = "/files";

	private static final Long FILE_ID = -1L;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private FileService fileService;

	@Test
	public void shouldMarkAsExpired() throws Exception {
		mockMvc.perform(patch(FILES_URL_PREFIX + "/{id}/mark_expired", FILE_ID));
		verify(fileService).markAsExpired(FILE_ID);
	}

	@Test
	public void shouldReturnAllFiles() throws Exception {
		FileInfoDTO fileInfoDTO = new FileInfoDTO();
		fileInfoDTO.setId(FILE_ID);
		when(fileService.getAll()).thenReturn(Arrays.asList(fileInfoDTO));
		
		mockMvc.perform(get(FILES_URL_PREFIX))
				.andExpect(status().isOk())
				.andExpect(content().json("[ { id: -1 } ]"));
	}

}
