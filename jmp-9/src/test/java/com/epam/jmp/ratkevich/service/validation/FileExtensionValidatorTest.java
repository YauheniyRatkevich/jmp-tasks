package com.epam.jmp.ratkevich.service.validation;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class FileExtensionValidatorTest {

	private static final String[] ALLOWED_EXTENSIONS = new String[] { "doc", "docx", "pdf", "txt", "jpeg", "jpg", "png", "bmp", "zip" };

	@Parameters(name = "{index}: shouldValidateExtension({0})")
	public static List<String> data() {
		return Arrays.asList("doc", "docx", "pdf", "txt", "jpeg", "NON_SUPPORTED");
	}

	@Parameter
	public String extension;

	@Test
	public void shouldValidateExtension() {
		assertEquals(testValidity(extension), FileExtensionValidator.validate(extension));
	}

	public boolean testValidity(String extension) {
		return Arrays.asList(ALLOWED_EXTENSIONS).contains(extension);
	}

}
