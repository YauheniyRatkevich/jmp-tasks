package com.epam.jmp.ratkevich.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.epam.jmp.ratkevich.dto.FileInfoDTO;
import com.epam.jmp.ratkevich.service.FileService;

@RestController
@RequestMapping("/files")
public class FileController {

	@Autowired
	private FileService fileService;

	@GetMapping
	public List<FileInfoDTO> getFiles() {
		return fileService.getAll();
	}

	@GetMapping("/{id}")
	public FileInfoDTO getFile(@PathVariable("id") Long id) {
		return fileService.getById(id);
	}

	@PostMapping
	public void uploadFile(@RequestParam("file") MultipartFile file) {
		try {
			final FileInfoDTO fileInfoDTO = new FileInfoDTO();
			fileInfoDTO.setFileName(file.getOriginalFilename());
			fileInfoDTO.setData(file.getBytes());
			fileService.save(fileInfoDTO);
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	@PatchMapping("/{id}/mark_expired")
	public void markExpired(@PathVariable("id") Long id) {
		fileService.markAsExpired(id);
	}

	@PatchMapping("/{id}/mark_not_expired")
	public void markNotExpired(@PathVariable("id") Long id) {
		fileService.markAsNotExpired(id);
	}

	@PatchMapping("/{id}")
	public void changeFileName(@PathVariable("id") Long id, @RequestBody FileInfoDTO fileInfoDTO) {
		fileInfoDTO.setId(id);
		fileService.changeFileName(fileInfoDTO);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Long id) {
		fileService.delete(id);
	}

	@GetMapping("/revisions/{num}")
	public Object revision(@PathVariable("num") Integer num) {
		return fileService.getRevision(num);
	}

	@GetMapping("/revisions")
	public List<?> revisions() {
		return fileService.getRevisions();
	}

	@GetMapping("/revisions/expiration")
	public List<?> revisionsOfExpiration() {
		return fileService.getRevisionsOfExpiration();
	}

}
