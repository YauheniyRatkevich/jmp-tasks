package com.epam.jmp.ratkevich.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Parent class of all domain entities.
 *
 * @param <T> - id class
 */
@MappedSuperclass
public abstract class AbstractDomainEntity<T extends Serializable> {

	@Id
	@GeneratedValue
	private T id;

	public T getId() {
		return id;
	}

	public void setId(T id) {
		this.id = id;
	}
}
