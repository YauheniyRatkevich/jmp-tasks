package com.epam.jmp.ratkevich.service;

import java.util.List;

import com.epam.jmp.ratkevich.dto.AbstractDomainDTO;

/**
 * Contains CRUD operations for {@link AbstractDomainDTO}
 *
 * @param <T> - real class representative of {@link AbstractDomainDTO}.
 */
public interface CrudService<T extends AbstractDomainDTO<?>> {

	default List<T> getAll() {
		throw new UnsupportedOperationException();
	}

	default T getById(Long id) {
		throw new UnsupportedOperationException();
	}

	default T update(T t) {
		throw new UnsupportedOperationException();
	}

	default T save(T t) {
		throw new UnsupportedOperationException();
	}

	default void delete(Long id) {
		throw new UnsupportedOperationException();
	}

}
