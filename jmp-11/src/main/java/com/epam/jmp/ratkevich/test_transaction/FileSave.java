package com.epam.jmp.ratkevich.test_transaction;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jmp.ratkevich.entity.FileInfo;
import com.epam.jmp.ratkevich.repository.FileRepository;

@Service
public class FileSave {

	@Autowired
	private FileRepository fileRepository;

	@Autowired
	private InterruptFileSave interruptFileSave;

	@Transactional
	public Long testRequired() {
		FileInfo savedFileInfo = fileRepository.save(getFileInfo(0));
		try {
			interruptFileSave.testRequired();
		} catch (Exception ex) {
			System.out.println("Required: inner fail. " + ex.getMessage());
		}

		return savedFileInfo.getId();
	}

	@Transactional
	public Long testRequiredNew() {
		FileInfo savedFileInfo = fileRepository.save(getFileInfo(1));
		try {
			interruptFileSave.testRequiredNew();
		} catch (Exception ex) {
			System.out.println("Required new: inner fail. " + ex.getMessage());
		}

		return savedFileInfo.getId();
	}

	@Transactional
	public Long testNested() {
		FileInfo savedFileInfo = fileRepository.save(getFileInfo(2));
		try {
			interruptFileSave.testNested();
		} catch (Exception ex) {
			System.out.println("Nested: inner fail. " + ex.getMessage());
		}

		return savedFileInfo.getId();
	}

	@Transactional
	public Long testMandatory() {
		FileInfo savedFileInfo = fileRepository.save(getFileInfo(3));
		try {
			interruptFileSave.testMandatory();
		} catch (Exception ex) {
			System.out.println("Mandatory: inner fail. " + ex.getMessage());
		}

		return savedFileInfo.getId();
	}

	@Transactional
	public Long testNotSupported() {
		FileInfo savedFileInfo = fileRepository.save(getFileInfo(4));
		try {
			interruptFileSave.testNotSupported();
		} catch (Exception ex) {
			System.out.println("NotSupported: inner fail. " + ex.getMessage());
		}

		return savedFileInfo.getId();
	}

	@Transactional
	public Long testNever() {
		FileInfo savedFileInfo = fileRepository.save(getFileInfo(5));
		try {
			interruptFileSave.testNever();
		} catch (Exception ex) {
			System.out.println("Never: inner fail. " + ex.getMessage());
		}

		return savedFileInfo.getId();
	}

	@Transactional
	public Long testSupports() {
		FileInfo savedFileInfo = fileRepository.save(getFileInfo(6));
		try {
			interruptFileSave.testSupports();
		} catch (Exception ex) {
			System.out.println("Supports: inner fail. " + ex.getMessage());
		}

		return savedFileInfo.getId();
	}

	private FileInfo getFileInfo(int testNum) {
		FileInfo fileInfo = new FileInfo();
		fileInfo.setFileName("file" + testNum);
		fileInfo.setExpirationDate(LocalDateTime.now().plusDays(1));
		fileInfo.setExpired(false);
		fileInfo.setData(new byte[] { 1, 2, 3 });
		return fileInfo;
	}
}
