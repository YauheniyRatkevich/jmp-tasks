package com.epam.jmp.ratkevich;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.epam.jmp.ratkevich.repository.FileRepository;
import com.epam.jmp.ratkevich.test_transaction.FileSave;

@SpringBootApplication
@EnableJpaAuditing
public class FileServiceWebApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(FileServiceWebApplication.class);
	}

	public static void main(String[] args) {
		// SpringApplication.run(FileServiceWebApplication.class, args);
		testPropagations(args);
	}

	private static void testPropagations(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(FileServiceWebApplication.class, args);
		FileSave fileSave = context.getBean(FileSave.class);
		FileRepository fileRepository = context.getBean(FileRepository.class);

		try {
			fileSave.testRequired();
		} catch (Exception ex) {
			System.out.println("Required wasn't committed. " + ex.getMessage());
		}

		System.out.println("-----");
		System.out.println(fileRepository.findOne(fileSave.testRequiredNew()));

		System.out.println("-----");
		System.out.println(fileRepository.findOne(fileSave.testNested()));

		System.out.println("-----");
		try {
			fileSave.testMandatory();
		} catch (Exception ex) {
			System.out.println("Mandatory wasn't committed. " + ex.getMessage());
		}

		System.out.println("-----");
		System.out.println(fileRepository.findOne(fileSave.testNotSupported()));

		System.out.println("-----");
		try {
			fileSave.testSupports();
		} catch (Exception ex) {
			System.out.println("Supports wasn't committed. " + ex.getMessage());
		}

		System.out.println("-----");
		System.out.println(fileRepository.findOne(fileSave.testNever()));
	}
}
