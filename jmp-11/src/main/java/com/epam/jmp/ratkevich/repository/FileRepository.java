package com.epam.jmp.ratkevich.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.jmp.ratkevich.entity.FileInfo;

/**
 * Contains operations for manipulations with files in DB (CRUD operations and some specific operations).
 */
public interface FileRepository extends JpaRepository<FileInfo, Long> {

	// @Modifying
	// @Query("UPDATE FileInfo fi SET fi.expired = true WHERE fi.id=:id")
	// void markAsExpired(@Param("id") Long id);

}
