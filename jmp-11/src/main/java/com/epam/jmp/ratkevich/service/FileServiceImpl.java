package com.epam.jmp.ratkevich.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jmp.ratkevich.converter.FileMetaDataConverter;
import com.epam.jmp.ratkevich.dto.FileInfoDTO;
import com.epam.jmp.ratkevich.entity.FileInfo;
import com.epam.jmp.ratkevich.repository.FileRepository;
import com.epam.jmp.ratkevich.service.validation.FileInfoValidator;

@Service
public class FileServiceImpl implements FileService {

	private static final int EXPIRATION_IN_DAYS = 1;

	@Autowired
	private FileRepository fileRepository;

	@Autowired
	private FileMetaDataConverter fileConverter;

	@Autowired
	private FileInfoValidator fileInfoValidator;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public FileInfoDTO save(FileInfoDTO sourceFileDTO) {
		fileInfoValidator.validate(sourceFileDTO);

		final FileInfo fileInfo = new FileInfo();
		fileInfo.setFileName(sourceFileDTO.getFileName());
		fileInfo.setExpirationDate(LocalDateTime.now().plusDays(EXPIRATION_IN_DAYS));
		fileInfo.setExpired(false);
		fileInfo.setData(sourceFileDTO.getData());
		return fileConverter.convert(fileRepository.save(fileInfo));
	}

	@Override
	public FileInfoDTO getById(Long id) {
		return fileConverter.convert(fileRepository.findOne(id));
	}

	@Override
	public List<FileInfoDTO> getAll() {
		return fileRepository.findAll().stream().map(fileConverter::convert).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public void markAsExpired(Long id) {
		FileInfo fileInfo = fileRepository.findOne(id);
		fileInfo.setExpired(true);
	}

	@Override
	@Transactional
	public void markAsNotExpired(Long id) {
		FileInfo fileInfo = fileRepository.findOne(id);
		fileInfo.setExpired(false);
	}

	@Override
	@Transactional
	public void changeFileName(FileInfoDTO fileInfoDTO) {
		fileInfoValidator.validateExtension(fileInfoDTO);

		FileInfo fileInfo = fileRepository.findOne(fileInfoDTO.getId());
		fileInfo.setFileName(fileInfoDTO.getFileName());
	}

	@Override
	public void delete(Long id) {
		fileRepository.delete(id);
	}

	@Override
	public Object getRevision(Integer num) {
		AuditReader reader = AuditReaderFactory.get(entityManager);
		AuditQuery query = reader.createQuery().forEntitiesAtRevision(FileInfo.class, FileInfo.class.getCanonicalName(), num, true);
		query.addOrder(AuditEntity.id().asc());
		return query.getSingleResult();
	}

	@Override
	public List<?> getRevisions() {
		AuditReader reader = AuditReaderFactory.get(entityManager);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(FileInfo.class, true, true);
		query.addOrder(AuditEntity.id().asc());
		return query.getResultList();
	}

	@Override
	public List<?> getRevisionsOfExpiration() {
		AuditReader reader = AuditReaderFactory.get(entityManager);
		AuditQuery query = reader.createQuery().forRevisionsOfEntity(FileInfo.class, true, true);
		query.addProjection(AuditEntity.property("expired"));
		query.addOrder(AuditEntity.id().asc());
		return query.getResultList();
	}
	

}
