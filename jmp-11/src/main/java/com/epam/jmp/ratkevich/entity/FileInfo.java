package com.epam.jmp.ratkevich.entity;

import java.time.LocalDateTime;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Contains information about a file within its metadata.
 * 
 * Represents mapping to FILE_INFO table in DB.
 * 
 */
@Entity
@Audited
@EntityListeners(AuditingEntityListener.class)
public class FileInfo extends AbstractDomainEntity<Long> {

	private String fileName;

	@CreatedDate
	private LocalDateTime uploadDate;

	private LocalDateTime expirationDate;

	private Boolean expired;

	@NotAudited
	@Column(columnDefinition = "BLOB(200M)")
	private byte[] data;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public LocalDateTime getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(LocalDateTime uploadDate) {
		this.uploadDate = uploadDate;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Boolean getExpired() {
		return expired;
	}

	public void setExpired(Boolean expired) {
		this.expired = expired;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "FileInfo [fileName=" + fileName + ", uploadDate=" + uploadDate + ", expirationDate=" + expirationDate + ", expired=" + expired + ", data="
				+ Arrays.toString(data) + ", getId()=" + getId() + "]";
	}

}
