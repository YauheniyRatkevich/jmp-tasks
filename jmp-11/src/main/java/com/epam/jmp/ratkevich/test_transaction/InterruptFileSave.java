package com.epam.jmp.ratkevich.test_transaction;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InterruptFileSave {

	@Transactional(propagation = Propagation.REQUIRED)
	public void testRequired() {
		throw new RuntimeException("inner Runtime");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void testRequiredNew() {
		throw new RuntimeException("inner Runtime");
	}

	@Transactional(propagation = Propagation.NESTED)
	public void testNested() {
		throw new RuntimeException("inner Runtime");
	}

	@Transactional(propagation = Propagation.MANDATORY)
	public void testMandatory() {
		throw new RuntimeException("inner Runtime");
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public void testNotSupported() {
		throw new RuntimeException("inner Runtime");
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	public void testSupports() {
		throw new RuntimeException("inner Runtime");
	}

	@Transactional(propagation = Propagation.NEVER)
	public void testNever() {
		throw new RuntimeException("inner Runtime");
	}

}
