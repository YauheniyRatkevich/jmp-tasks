package com.epam.jmp.ratkevich;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;

import com.epam.jmp.ratkevich.entity.FileInfo;
import com.epam.jmp.ratkevich.repository.util.ConnectionUtils;
import com.epam.jmp.ratkevich.service.FileService;
import com.epam.jmp.ratkevich.service.FileServiceImpl;

public class Main {

	private static final long ID = 40;

	private static final String SOURCE_FOLDER = "SOURCE";

	private static final String DEST_FOLDER = "DEST";

	private static final long SIZE_LIMIT = 200 * 1024; // 200M

	private static final long EXPIRATION_IN_DAYS = 1; // 1 day

	public static void main(String[] args) throws SQLException, IOException {
		app_init();

		cleanCreateDestFolder();

		final FileService fileService = new FileServiceImpl();
		fileService.saveAll(Paths.get(SOURCE_FOLDER), SIZE_LIMIT, EXPIRATION_IN_DAYS);

		FileInfo savedFile = fileService.findOne(ID);
		Files.write(Paths.get(DEST_FOLDER + "\\" + savedFile.getFileName()), savedFile.getData());

		fileService.markAsExpired(savedFile.getId());

		System.out.println(fileService.getUploadDate(savedFile.getId()));

		app_destroy();
	}

	private static void cleanCreateDestFolder() throws IOException {
		Path newdir = Paths.get(DEST_FOLDER);
		deleteDirectory(newdir.toFile());
		if (!Files.exists(newdir)) {
			Files.createDirectory(newdir);
		}
	}

	private static void app_init() {
		ConnectionUtils.getInstance().init();
	}

	private static void app_destroy() {
		ConnectionUtils.getInstance().destroy();
	}

	private static void deleteDirectory(File dir) {
		if (dir.exists() && dir.isDirectory()) {
			String[] files = dir.list();
			for (int i = 0, len = files.length; i < len; i++) {
				File f = new File(dir, files[i]);
				if (f.isDirectory()) {
					deleteDirectory(f);
				} else {
					f.delete();
				}
			}
			dir.delete();
		}
	}
}
