package com.epam.jmp.ratkevich.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;

import com.epam.jmp.ratkevich.entity.FileInfo;
import com.epam.jmp.ratkevich.repository.FileRepository;
import com.epam.jmp.ratkevich.repository.FileRepositoryImpl;

public class FileServiceImpl implements FileService {

	private static final int BYTES_IN_KILOBYTE = 1024;

	private FileRepository fileRepository = new FileRepositoryImpl();

	public void save(Path sourceFile, Long expirationInDays) {
		try {
			final FileInfo fileInfo = new FileInfo();
			fileInfo.setFileName(sourceFile.toFile().getName());
			fileInfo.setUploadDate(LocalDateTime.now());
			fileInfo.setExpirationDate(LocalDateTime.now().plusDays(expirationInDays));
			fileInfo.setExpired(false);
			fileInfo.setData(Files.readAllBytes(sourceFile));
			fileRepository.save(fileInfo);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void saveAll(Path sourceDir, Long sizeLimit, Long expirationInDays) throws IOException {
		Files.walk(sourceDir).filter(Files::isRegularFile).filter(path -> {
			boolean isNormalSize = false;
			try {
				isNormalSize = Files.size(path) < sizeLimit * BYTES_IN_KILOBYTE;
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			if (!isNormalSize) {
				System.err.println("File size is more than " + sizeLimit + " bytes");
			}
			return isNormalSize;
		}).parallel().forEach(file -> save(file, expirationInDays));
	}

	@Override
	public FileInfo findOne(Long id) {
		return fileRepository.findOne(id);
	}

	@Override
	public void markAsExpired(Long id) {
		fileRepository.markAsExpired(id);
	}

	@Override
	public LocalDateTime getUploadDate(Long id) {
		return fileRepository.getUploadDate(id);
	}

}
