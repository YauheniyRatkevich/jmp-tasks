package com.epam.jmp.ratkevich.dto;

import java.io.File;
import java.time.LocalDateTime;

/**
 * Contains information about a file within its metadata.
 *
 */
public class FileInfoDTO extends AbstractDomainDTO<Long> {

	private LocalDateTime uploadDate;

	private LocalDateTime expirationDate;

	private boolean isExpired;

	private File file;

	public LocalDateTime getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(LocalDateTime uploadDate) {
		this.uploadDate = uploadDate;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}

	public boolean isExpired() {
		return isExpired;
	}

	public void setExpired(boolean isExpired) {
		this.isExpired = isExpired;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

}