package com.epam.jmp.ratkevich.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Semaphore;

public class FileServiceImplSemaphore extends FileServiceImpl {

	public FileServiceImplSemaphore(String destFolder) {
		super(destFolder);
	}

	@Override
	public void saveAll(Path sourceDir) throws IOException {
		final Semaphore semaphore = new Semaphore(threadCountLimit);
		Files.walk(sourceDir).filter(Files::isRegularFile).forEach(sourceFile -> {
			new Thread(() -> {
				try {
					semaphore.acquire();
					save(sourceFile);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				} finally {
					// semaphore.release(); !livelock!
				}
			}).start();
		});
	}
}
