package com.epam.jmp.ratkevich.service.validation;

import java.util.Random;

import javax.validation.ValidationException;

import org.junit.Before;
import org.junit.Test;

import com.epam.jmp.ratkevich.dto.FileInfoDTO;

public class FileInfoValidatorTest {

	private static final String FILENAME_WRONG_EXT = "1.wrongext!";

	private static final int OVERSIZED_DATA_LENGTH = 1 + 200 * 1024 * 1024;

	private FileInfoValidator fileInfoValidator = new FileInfoValidator();;

	private FileInfoDTO fileInfoDTO = new FileInfoDTO();

	@Before
	public void setup() {
		fileInfoDTO.setData(new byte[] { -1, 0, 1 });
		fileInfoDTO.setFileName("1.docx");
	}

	@Test
	public void shouldBeOkWhenValidate() {
		fileInfoValidator.vaildate(fileInfoDTO);
	}

	@Test(expected = ValidationException.class)
	public void shouldThrowValidationExceptionForNonSupportedExtension() {
		fileInfoDTO.setFileName(FILENAME_WRONG_EXT);
		fileInfoValidator.vaildate(fileInfoDTO);
	}

	@Test(expected = ValidationException.class)
	public void shouldThrowValidationExceptionForHugeFiles() {
		byte[] bytes = new byte[OVERSIZED_DATA_LENGTH];
		new Random().nextBytes(bytes);
		fileInfoDTO.setData(bytes);
		fileInfoValidator.vaildate(fileInfoDTO);
	}

}
